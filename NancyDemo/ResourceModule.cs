﻿using Nancy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NancyDemo
{
    public class WebService : NancyModule
    {


        public  WebService():base()
        {

            Get["/{category}"] = parameters => "My category is " + parameters.category;

            Get["/sayhello"] = _ => "Hello from Nancy";
             
        }

    }
 
}
